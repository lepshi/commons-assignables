package net.lepshi.commons.assignables.annotations;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static java.beans.Introspector.decapitalize;
import static java.util.Objects.nonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static javax.lang.model.util.ElementFilter.constructorsIn;
import static javax.lang.model.util.ElementFilter.methodsIn;

class BeanTypeInspector {

    private final TypeElement                typeElement;
    private final ProcessingEnvironment      processingEnv;
    private final AnnotationProcessingHelper helper;


    BeanTypeInspector(TypeElement typeElement, ProcessingEnvironment processingEnv) {
        this.typeElement = typeElement;
        this.processingEnv = processingEnv;
        this.helper = new AnnotationProcessingHelper(processingEnv);
    }

    Collection<PropertyInfo> findProperties() {
        final List<ExecutableElement> allMethods = methodsIn(typeElement.getEnclosedElements());
        final Map<String, PropertyInfo> properties = allMethods.stream()
                                                               .filter(this::isGetter)
                                                               .map(this::toReadablePropertyInfo)
                                                               .collect(toMap(PropertyInfo::name,
                                                                              identity()));
        allMethods.stream()
                  .filter(this::isSetter)
                  .forEach(setterMethod -> {
                      final String propName = propertyNameOf(setterMethod).get();
                      Optional.ofNullable(properties.get(propName))
                              .ifPresent(propInfo -> propInfo.addSetter(setterMethod));
                  });

        return properties.values();
    }

    boolean hasNoArgsConstructor() {
        return constructorsIn(typeElement.getEnclosedElements()).stream()
                                                                .anyMatch(this::hasNoArgs);
    }

    boolean isAnnotatedWith(Class<? extends Annotation> annotationType) {
        return nonNull(typeElement.getAnnotation(annotationType));
    }

    private boolean hasNoArgs(ExecutableElement executableElem) {
        return executableElem.getParameters()
                             .isEmpty();
    }

    private boolean isGetter(ExecutableElement method) {
        if (isPropertyAccessor(method)) {
            return !isMethodWithPrefix(method, "set");
        }
        return false;
    }

    private boolean isSetter(ExecutableElement method) {
        if (isPropertyAccessor(method)) {
            return isMethodWithPrefix(method, "set");
        }
        return false;
    }

    private boolean isMethodWithPrefix(ExecutableElement method,
                                       String prefix) {
        return method.getSimpleName()
                     .toString()
                     .startsWith(prefix);
    }

    private boolean isPropertyAccessor(ExecutableElement method) {
        return propertyNameOf(method).isPresent();
    }

    private Optional<String> propertyNameOf(ExecutableElement method) {
        if (hasNoArgs(method)) {
            return getterPropertyNameOf(method);
        }
        if (method.getReturnType().getKind() == TypeKind.VOID &&
                method.getParameters().size() == 1) {
            return setterPropertyNameOf(method);
        }
        return Optional.empty();
    }

    private Optional<String> getterPropertyNameOf(ExecutableElement getterStyleMethod) {
        final String name = getterStyleMethod.getSimpleName()
                                             .toString();
        if (name.startsWith("get")) {
            return Optional.of(decapitalize(name.substring(3)));

        } else {
            final TypeMirror returnType = getterStyleMethod.getReturnType();
            final TypeMirror TYPE_BOOLEAN = processingEnv.getElementUtils()
                                                         .getTypeElement(Boolean.class.getName())
                                                         .asType();
            if (returnType.getKind() == TypeKind.BOOLEAN ||
                    TYPE_BOOLEAN.equals(returnType)) {
                if (name.startsWith("is")) {
                    return Optional.of(decapitalize(name.substring(2)));
                } else if (name.startsWith("has")) {
                    return Optional.of(decapitalize(name.substring(3)));
                }
            }
        }
        return Optional.empty();
    }

    private Optional<String> setterPropertyNameOf(ExecutableElement setterStyleMethod) {
        final String name = setterStyleMethod.getSimpleName()
                                             .toString();
        if (name.startsWith("set")) {
            return Optional.of(decapitalize(name.substring(3)));
        }
        return Optional.empty();
    }

    private PropertyInfo toReadablePropertyInfo(ExecutableElement getterMethod) {
        return new PropertyInfo(propertyNameOf(getterMethod).get(),
                                getterMethod);
    }


    static class PropertyInfo {
        private final String            name;
        private final TypeMirror        type;
        private final ExecutableElement getterMethod;
        private       ExecutableElement setterMethod;

        private PropertyInfo(String name, ExecutableElement getterMethod) {
            this.name = name;
            this.getterMethod = getterMethod;
            this.type = getterMethod.getReturnType();
            checkArgument(getterMethod.getParameters().isEmpty(),
                          "Property getter must have zero parameters: {}",
                          getterMethod);
        }

        private void addSetter(ExecutableElement setterMethod) {
            this.setterMethod = setterMethod;
        }

        public String name() {
            return name;
        }

        public TypeMirror type() {
            return type;
        }

        public boolean isWritable() {
            return nonNull(setterMethod);
        }

        public ExecutableElement getterMethod() {
            return getterMethod;
        }

        public ExecutableElement setterMethod() {
            return setterMethod;
        }
    }
}
