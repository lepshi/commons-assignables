package net.lepshi.commons.assignables.annotations;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static com.squareup.javapoet.TypeSpec.classBuilder;
import static java.lang.String.format;

@SupportedAnnotationTypes(
        AssignablesCollectingProcessor.ASSIGNABLES_ANNOTATIONS_PACKAGE + ".Assignables")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class AssignablesCollectingProcessor extends AbstractProcessor {

    static final String ASSIGNABLES_ANNOTATIONS_PACKAGE = "net.lepshi.commons.assignables.annotations";

    private static final AtomicInteger outputFileCounter = new AtomicInteger();

    @Override
    public boolean process(Set<? extends TypeElement> annotations,
                           RoundEnvironment roundEnv) {

        new InfoClassSourceGenerator(roundEnv, annotations).generate();
        return true;
    }

    private class InfoClassSourceGenerator {

        private final RoundEnvironment           roundEnv;
        private final Set<? extends TypeElement> annotations;
        private final TypeSpec.Builder           classSpecBuilder;
        private final AtomicInteger              fieldsCounter = new AtomicInteger();
        private final AnnotationProcessingHelper helper        = new AnnotationProcessingHelper(processingEnv);

        InfoClassSourceGenerator(RoundEnvironment roundEnv,
                                 Set<? extends TypeElement> annotations) {
            this.roundEnv = roundEnv;
            this.annotations = annotations;
            this.classSpecBuilder = classBuilder("TypesWithAssignablesInfo" + outputFileCounter.getAndIncrement());
        }


        void generate() {
            classSpecBuilder.addAnnotation(TypesToProcess.class);

            annotations.stream()
                       .flatMap(annotation -> roundEnv.getElementsAnnotatedWith(annotation).stream())
                       .forEach(this::addAnnotatedClass);

            if (fieldsCounter.get() > 0) {
                helper.writeClassToJavaFile(classSpecBuilder.build(),
                                            ASSIGNABLES_ANNOTATIONS_PACKAGE,
                                            javaFile -> {
                                            });
            }
        }

        private void addAnnotatedClass(Element annotatedElement) {
            try {
                final TypeElement annotatedClassElement = helper.toClassElement(annotatedElement);
                final TypeName annotatedClassName = TypeName.get(annotatedClassElement.asType());
                classSpecBuilder.addField(annotatedClassName,
                                          nextFieldName());
            } catch (RuntimeException e) {
                helper.printError(format("Error adding element '%s' to info file", annotatedElement), e);
            }
        }

        private String nextFieldName() {
            return format("t%04d", fieldsCounter.getAndIncrement());
        }
    }
}