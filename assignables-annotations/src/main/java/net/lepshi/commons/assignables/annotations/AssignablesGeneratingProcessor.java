package net.lepshi.commons.assignables.annotations;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes(
        AssignablesCollectingProcessor.ASSIGNABLES_ANNOTATIONS_PACKAGE + ".TypesToProcess")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class AssignablesGeneratingProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations,
                           RoundEnvironment roundEnv) {

        final AnnotationProcessingHelper helper = new AnnotationProcessingHelper(processingEnv);
        final InfoClassProcessor processor = new InfoClassProcessor(roundEnv, helper);
        annotations.stream()
                   .flatMap(annotation -> roundEnv.getElementsAnnotatedWith(annotation).stream())
                   .map(helper::toClassElement)
                   .forEach(processor::processInfoClass);

        return true;
    }


    private class InfoClassProcessor {

        private final RoundEnvironment           roundEnv;
        private final AnnotationProcessingHelper helper;

        InfoClassProcessor(RoundEnvironment roundEnv, AnnotationProcessingHelper helper) {
            this.roundEnv = roundEnv;
            this.helper = helper;
        }


        void processInfoClass(TypeElement infoClassElement) {
            try {
                processingEnv.getElementUtils()
                             .getAllMembers(infoClassElement)
                             .stream()
                             .filter(this::isField)
                             .map(helper::getFieldType)
                             .forEach(this::processAssignableType);
            } catch (RuntimeException e) {
                helper.printError("Failed to process AssignableInfoClass '" + infoClassElement.getQualifiedName() + "'", e);
            }
        }

        private void processAssignableType(TypeElement typeElement) {
            helper.checkTypeAnnotation(typeElement, Assignables.class);

            new AssignableClassSourceGenerator(typeElement,
                                               roundEnv,
                                               processingEnv).generate();
        }

        private boolean isField(Element element) {
            return element.getKind().isField();
        }
    }

}