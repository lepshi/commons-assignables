package net.lepshi.commons.assignables.annotations;

class AssignableProcessingException extends RuntimeException {

    AssignableProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
