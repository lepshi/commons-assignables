package net.lepshi.commons.assignables.annotations;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static java.util.Objects.isNull;

class AnnotationProcessingHelper {

    private final ProcessingEnvironment processingEnv;

    AnnotationProcessingHelper(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
    }

    void checkTypeAnnotation(TypeElement typeElement,
                             Class<? extends Annotation> annotationType) {
        if (isNull(typeElement.getAnnotation(annotationType))) {
            printError(format("Class '%s' is not annotated with @%s",
                              typeElement.getQualifiedName(),
                              annotationType.getName()));
        }
    }

    TypeElement toTypeElement(TypeName typeName) {
        return checkNotNull(processingEnv.getElementUtils().getTypeElement(typeName.toString()),
                            "No TypeElement for: %s", typeName);
    }

    TypeElement getFieldType(Element field) {
        TypeMirror fieldTypeMirror = field.asType();
        return (TypeElement) processingEnv.getTypeUtils()
                                          .asElement(fieldTypeMirror);
    }

    TypeElement toClassElement(Element element) {
        if (!element.getKind().isClass()) {
            printWarning(format("Expected element to be a class: %s", element));
        }
        return (TypeElement) element;
    }

    void printNote(String msg) {
        processingEnv.getMessager()
                     .printMessage(Diagnostic.Kind.NOTE, msg);
    }

    void printWarning(String msg) {
        processingEnv.getMessager()
                     .printMessage(Diagnostic.Kind.MANDATORY_WARNING, msg);
    }

    void printError(String msg) {
        processingEnv.getMessager()
                     .printMessage(Diagnostic.Kind.ERROR, msg);
    }

    void printError(String msg, Throwable cause) {
        processingEnv.getMessager()
                     .printMessage(Diagnostic.Kind.ERROR, format("%s: %s", msg, cause));
        cause.printStackTrace();
    }


    void writeClassToJavaFile(TypeSpec classSpec, String packageName, Consumer<JavaFile.Builder> javaFileCustomizer) {
        final JavaFile.Builder javaFileBuilder = JavaFile.builder(packageName, classSpec);
        javaFileCustomizer.accept(javaFileBuilder);
        final JavaFile javaFile = javaFileBuilder.indent("    ")
                                                 .build();
        try {
            final JavaFileObject javaFileObject = processingEnv.getFiler()
                                                               .createSourceFile(packageName + "." + classSpec.name);
            try (Writer writer = javaFileObject.openWriter()) {
                javaFile.writeTo(writer);
            }
        } catch (IOException e) {
            printError("Error writing info file", e);
        }
    }

    TypeMirror asTypeMirror(Class<?> clazz) {
        return processingEnv.getElementUtils()
                            .getTypeElement(clazz.getName())
                            .asType();
    }
}