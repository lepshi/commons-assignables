package net.lepshi.commons.assignables.annotations;

import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.WildcardTypeName;
import net.lepshi.commons.assignables.Assignable;

import static com.squareup.javapoet.TypeName.OBJECT;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;

abstract class AssignableFactoryMethodBuilder<B extends AssignableFactoryMethodBuilder> {

    private static final ClassName ASSIGNABLE_CLASS_NAME = ClassName.get(Assignable.class);

    protected final ClassName          parentClassName;
    protected final MethodSpec.Builder spec;
    protected final TypeName           itemTypeName;


    protected CodeBlock createAssignableFactoryCode;
    protected CodeBlock assignableFromFactoryMethodCall;

    static ItemAssignableFactoryMethodBuilder assignableFactory(String methodName,
                                                                ClassName parentClassName,
                                                                TypeName itemTypeName) {
        return new ItemAssignableFactoryMethodBuilder(methodName,
                                                      parentClassName,
                                                      itemTypeName);
    }

    protected AssignableFactoryMethodBuilder(String methodName,
                                             ClassName parentClassName,
                                             TypeName itemTypeName
    ) {
        this.parentClassName = parentClassName;
        this.spec = MethodSpec.methodBuilder(methodName)
                              .addModifiers(PUBLIC, STATIC);
        this.itemTypeName = itemTypeName;
    }


    MethodSpec buildMethod() {
        return spec.addCode("return ")
                   .addStatement(CodeBlock.builder()
                                          .add(createAssignableFactoryCode).add("\n")
                                          .indent().indent()
                                          .add(".").add(assignableFromFactoryMethodCall)
                                          .unindent().unindent()
                                          .build())
                   .build();
    }

    MethodSpec buildMethodWithNestedFields() {

        spec.addAnnotation(SafeVarargs.class);
        spec.addParameter(ArrayTypeName.of(ParameterizedTypeName.get(ASSIGNABLE_CLASS_NAME,
                                                                     itemTypeName.box(),
                                                                     WildcardTypeName.subtypeOf(OBJECT))), "fields").varargs();
        return spec.addCode("return ")
                   .addStatement(CodeBlock.builder()
                                          .add(createAssignableFactoryCode).add("\n")
                                          .indent().indent()
                                          .add(".").add(assignableFromFactoryMethodCall)
                                          .add("\n")
                                          .add(".withFields(fields)")
                                          .unindent().unindent()
                                          .build())
                   .build();
    }
}
