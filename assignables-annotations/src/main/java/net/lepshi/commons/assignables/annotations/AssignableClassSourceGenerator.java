package net.lepshi.commons.assignables.annotations;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import net.lepshi.commons.assignables.AssignableFactory;
import net.lepshi.commons.assignables.NoParent;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.squareup.javapoet.TypeSpec.classBuilder;
import static java.beans.Introspector.decapitalize;
import static java.lang.String.format;
import static javax.lang.model.element.Modifier.PUBLIC;
import static net.lepshi.commons.assignables.annotations.AssignableFactoryMethodBuilder.assignableFactory;
import static org.apache.commons.lang3.StringUtils.capitalize;

class AssignableClassSourceGenerator {

    private static final ClassName NO_PARENT_CLASS_NAME = ClassName.get(NoParent.class);
    private static final ClassName MOCKITO_CLASS_NAME   = ClassName.get("org.mockito", "Mockito");

    private final TypeElement                underlyingTypeElement;
    private final ClassName                  underlyingClassName;
    private final RoundEnvironment           roundEnv;
    private final ProcessingEnvironment      processingEnv;
    private final AnnotationProcessingHelper helper;
    private final TypeSpec.Builder           classSpecBuilder;


    AssignableClassSourceGenerator(TypeElement underlyingTypeElement,
                                   RoundEnvironment roundEnv,
                                   ProcessingEnvironment processingEnv) {
        this.underlyingTypeElement = underlyingTypeElement;
        this.underlyingClassName = ClassName.get(underlyingTypeElement);
        this.roundEnv = roundEnv;
        this.processingEnv = processingEnv;
        this.helper = new AnnotationProcessingHelper(processingEnv);
        this.classSpecBuilder = classBuilder("Assignable" + underlyingClassName.simpleName());
    }

    void generate() {

        classSpecBuilder.addMethod(assignableFactory("_" + decapitalize(underlyingClassName.simpleName()),
                                                     NO_PARENT_CLASS_NAME,
                                                     underlyingClassName)
                                           .itemFromParam()
                                           .unassignable()
                                           .buildMethod());

        if (isAssignableBean(underlyingTypeElement.asType())) {
            classSpecBuilder.addMethod(assignableFactory("_" + decapitalize(underlyingClassName.simpleName()),
                                                         NO_PARENT_CLASS_NAME,
                                                         underlyingClassName)
                                               .itemFromCode(CodeBlock.builder()
                                                                      .add("new ")
                                                                      .add(underlyingClassName.reflectionName())
                                                                      .add("()")
                                                                      .build())
                                               .unassignable()
                                               .buildMethodWithNestedFields());

            classSpecBuilder.addMethod(assignableFactory("_mock" + capitalize(underlyingClassName.simpleName()),
                                                         NO_PARENT_CLASS_NAME,
                                                         underlyingClassName)
                                               .itemFromCode(CodeBlock.builder()
                                                                      .add("mock(")
                                                                      .add(underlyingClassName.reflectionName())
                                                                      .add(".class)")
                                                                      .build())
                                               .unassignable()
                                               .buildMethodWithNestedFields());
        }

        final BeanTypeInspector inspector = new BeanTypeInspector(underlyingTypeElement, processingEnv);
        inspector.findProperties()
                 .forEach(prop -> {

                     if (isPropertyAssignable(prop, Collection.class)) {
                         final Class<? extends Collection> collectionClazz = getDefaultImplementationOfCollectionType(prop);
                         final TypeMirror collectionEntryType = ((DeclaredType) prop.type()).getTypeArguments().get(0);
                         final ItemAssignableFactoryMethodBuilder methodBuilder = assignableFactory(toSingular(prop.name(), false), //method name
                                                                                                    underlyingClassName,
                                                                                                    TypeName.get(collectionEntryType));
                         methodBuilder.itemFromParam();
                         methodBuilder.assignableToCollection(prop.name(),
                                                              simpleNameOf(prop.getterMethod()),
                                                              simpleNameOf(prop.setterMethod()),
                                                              ClassName.get(collectionClazz));
                         classSpecBuilder.addMethod(methodBuilder.buildMethod());

                         if (isAssignableBean(collectionEntryType)) {
                             final ItemAssignableFactoryMethodBuilder realWithNestedFieldsBuilder = assignableFactory(toSingular(prop.name(), false), //method name
                                                                                                                      underlyingClassName,
                                                                                                                      TypeName.get(collectionEntryType));
                             realWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
                                                                               .add("new ")
                                                                               .add(TypeName.get(collectionEntryType).box().toString())
                                                                               .add("()")
                                                                               .build());
                             realWithNestedFieldsBuilder.assignableToCollection(prop.name(),
                                                                                simpleNameOf(prop.getterMethod()),
                                                                                simpleNameOf(prop.setterMethod()),
                                                                                ClassName.get(collectionClazz));
                             classSpecBuilder.addMethod(realWithNestedFieldsBuilder.buildMethodWithNestedFields());

                             final ItemAssignableFactoryMethodBuilder mockWithNestedFieldsBuilder = assignableFactory(toSingular(prop.name(), true), //method name
                                                                                                                      underlyingClassName,
                                                                                                                      TypeName.get(collectionEntryType));
                             mockWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
                                                                               .add("mock(")
                                                                               .add(TypeName.get(collectionEntryType).box().toString())
                                                                               .add(".class)")
                                                                               .build());
                             mockWithNestedFieldsBuilder.assignableToCollection(prop.name(),
                                                                                simpleNameOf(prop.getterMethod()),
                                                                                simpleNameOf(prop.setterMethod()),
                                                                                ClassName.get(collectionClazz));
                             classSpecBuilder.addMethod(mockWithNestedFieldsBuilder.buildMethodWithNestedFields());
                         }

                     } else if (isPropertyAssignable(prop, Map.class)) {
//                         final Class<? extends Map> mapClazz = LinkedHashMap.class;
//                         final List<? extends TypeMirror> mapTypeArguments = ((DeclaredType) prop.type()).getTypeArguments();
//                         final TypeMirror keyType = mapTypeArguments.get(0);
//                         final TypeMirror valueType = mapTypeArguments.get(1);
//                         final AssignableFactoryMethodBuilder methodBuilder = assignableFactory(toSingular(prop.name(), false), //method name
//                                                                                                underlyingClassName,
//                                                                                                TypeName.get(mapEntryType));
//                         methodBuilder.itemFromParam();
//                         methodBuilder.assignableToCollection(prop.name(),
//                                                              simpleNameOf(prop.getterMethod()),
//                                                              simpleNameOf(prop.setterMethod()),
//                                                              ClassName.get(mapClazz));
//                         classSpecBuilder.addMethod(methodBuilder.buildMethod());
//
//                         if (isAssignableBean(mapEntryType)) {
//                             final AssignableFactoryMethodBuilder realWithNestedFieldsBuilder = assignableFactory(toSingular(prop.name(), false), //method name
//                                                                                                                  underlyingClassName,
//                                                                                                                  TypeName.get(mapEntryType));
//                             realWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
//                                                                               .add("new ")
//                                                                               .add(TypeName.get(mapEntryType).box().toString())
//                                                                               .add("()")
//                                                                               .build());
//                             realWithNestedFieldsBuilder.assignableToCollection(prop.name(),
//                                                                                simpleNameOf(prop.getterMethod()),
//                                                                                simpleNameOf(prop.setterMethod()),
//                                                                                ClassName.get(mapClazz));
//                             classSpecBuilder.addMethod(realWithNestedFieldsBuilder.buildMethodWithNestedFields());
//
//                             final AssignableFactoryMethodBuilder mockWithNestedFieldsBuilder = assignableFactory(toSingular(prop.name(), true), //method name
//                                                                                                                  underlyingClassName,
//                                                                                                                  TypeName.get(mapEntryType));
//                             mockWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
//                                                                               .add("mock(")
//                                                                               .add(TypeName.get(mapEntryType).box().toString())
//                                                                               .add(".class)")
//                                                                               .build());
//                             mockWithNestedFieldsBuilder.assignableToCollection(prop.name(),
//                                                                                simpleNameOf(prop.getterMethod()),
//                                                                                simpleNameOf(prop.setterMethod()),
//                                                                                ClassName.get(mapClazz));
//                             classSpecBuilder.addMethod(mockWithNestedFieldsBuilder.buildMethodWithNestedFields());
//                         }

                     } else {

                         final ItemAssignableFactoryMethodBuilder methodBuilder = assignableFactory(prop.name(), //method name
                                                                                                    underlyingClassName,
                                                                                                    TypeName.get(prop.type()));
                         methodBuilder.itemFromParam();
                         methodBuilder.assignableToField(prop.name(),
                                                         simpleNameOf(prop.getterMethod()),
                                                         simpleNameOf(prop.setterMethod()));
                         classSpecBuilder.addMethod(methodBuilder.buildMethod());

                         if (isAssignableBean(prop.type())) {
                             final ItemAssignableFactoryMethodBuilder realWithNestedFieldsBuilder = assignableFactory(prop.name(), //method name
                                                                                                                      underlyingClassName,
                                                                                                                      TypeName.get(prop.type()));
                             realWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
                                                                               .add("new ")
                                                                               .add(TypeName.get(prop.type()).box().toString())
                                                                               .add("()")
                                                                               .build());
                             realWithNestedFieldsBuilder.assignableToField(prop.name(),
                                                                           simpleNameOf(prop.getterMethod()),
                                                                           simpleNameOf(prop.setterMethod()));
                             classSpecBuilder.addMethod(realWithNestedFieldsBuilder.buildMethodWithNestedFields());

                             final ItemAssignableFactoryMethodBuilder mockWithNestedFieldsBuilder = assignableFactory("mock" + capitalize(prop.name()), //method name
                                                                                                                      underlyingClassName,
                                                                                                                      TypeName.get(prop.type()));
                             mockWithNestedFieldsBuilder.itemFromCode(CodeBlock.builder()
                                                                               .add("mock(")
                                                                               .add(TypeName.get(prop.type()).box().toString())
                                                                               .add(".class)")
                                                                               .build());
                             mockWithNestedFieldsBuilder.assignableToField(prop.name(),
                                                                           simpleNameOf(prop.getterMethod()),
                                                                           simpleNameOf(prop.setterMethod()));
                             classSpecBuilder.addMethod(mockWithNestedFieldsBuilder.buildMethodWithNestedFields());
                         }
                     }

                 });

        PackageElement packageElement = processingEnv.getElementUtils().getPackageElement(underlyingClassName.packageName());
        classSpecBuilder.addModifiers(PUBLIC);
        helper.writeClassToJavaFile(classSpecBuilder.build(),
                                    packageElement.getQualifiedName().toString(),
                                    javaFile -> {
                                        javaFile.addStaticImport(AssignableFactory.class, "item");
                                        javaFile.addStaticImport(MOCKITO_CLASS_NAME, "mock");
                                    });
    }

    private boolean isAssignableBean(TypeMirror typeMirror) {
        final BeanTypeInspector beanTypeInspector = new BeanTypeInspector(helper.toTypeElement(TypeName.get(typeMirror).box()),
                                                                          processingEnv);
        return beanTypeInspector.hasNoArgsConstructor() &&
                beanTypeInspector.isAnnotatedWith(Assignables.class) &&
                !beanTypeInspector.findProperties().isEmpty();
    }

    private Class<? extends Collection> getDefaultImplementationOfCollectionType(BeanTypeInspector.PropertyInfo prop) {
        if (isPropertyAssignable(prop, Set.class)) {
            return LinkedHashSet.class;

        } else if (isPropertyAssignable(prop, List.class)) {
            return ArrayList.class;
        }
        throw new IllegalArgumentException("Unsupported java.util.Collection implementation: " + prop.type());
    }

    private boolean isPropertyAssignable(BeanTypeInspector.PropertyInfo prop, Class<?> targetType) {
        final TypeMirror propTypeMirror = erasure(prop.type());
        final TypeMirror targetTypeMirror = erasure(helper.asTypeMirror(targetType));
        return processingEnv.getTypeUtils()
                            .isAssignable(propTypeMirror,
                                          targetTypeMirror);
    }

    private TypeMirror erasure(TypeMirror typeMirror) {
        return processingEnv.getTypeUtils().erasure(typeMirror);
    }

    private static String simpleNameOf(ExecutableElement method) {
        return Optional.ofNullable(method)
                       .map(ExecutableElement::getSimpleName)
                       .map(Name::toString)
                       .orElse(null);
    }

    private static String toSingular(String pluralName, boolean isMock) {
        if (isMock) {
            return format("oneOfMock%s", capitalize(pluralName));
        } else {
            return format("oneOf%s", capitalize(pluralName));
        }
    }
}
