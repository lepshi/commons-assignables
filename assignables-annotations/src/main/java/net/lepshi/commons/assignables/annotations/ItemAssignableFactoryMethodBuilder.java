package net.lepshi.commons.assignables.annotations;

import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.WildcardTypeName;
import net.lepshi.commons.assignables.Assignable;

import static com.squareup.javapoet.TypeName.OBJECT;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;

class ItemAssignableFactoryMethodBuilder extends AssignableFactoryMethodBuilder<ItemAssignableFactoryMethodBuilder> {

    private static final ClassName ASSIGNABLE_CLASS_NAME = ClassName.get(Assignable.class);

    private final ClassName          parentClassName;
    private final MethodSpec.Builder spec;

    private CodeBlock createItemCode;
    private CodeBlock assignableFromItemMethodCall;

    ItemAssignableFactoryMethodBuilder(String methodName,
                                       ClassName parentClassName,
                                       TypeName itemTypeName) {

        super(methodName, parentClassName, itemTypeName);

        this.parentClassName = parentClassName;
        this.spec = MethodSpec.methodBuilder(methodName)
                              .addModifiers(PUBLIC, STATIC)
                              .returns(ParameterizedTypeName.get(ASSIGNABLE_CLASS_NAME,
                                                                 parentClassName,
                                                                 itemTypeName.box()))
                              .addJavadoc(" $1L # ($2L) $3L\n",
                                          parentClassName.simpleName(),
                                          itemTypeName,
                                          methodName
                              );
    }

    ItemAssignableFactoryMethodBuilder itemFromParam() {
        spec.addParameter(itemTypeName, "val");
        createItemCode = CodeBlock.of("item(val)");
        return this;
    }

    ItemAssignableFactoryMethodBuilder itemFromCode(CodeBlock createInstanceCode) {
        this.createItemCode = CodeBlock.builder()
                                       .add("item(")
                                       .add(createInstanceCode)
                                       .add(")")
                                       .build();
        return this;
    }

    ItemAssignableFactoryMethodBuilder reflectivelyAssignableToField(String fieldName) {
        assignableFromItemMethodCall = CodeBlock.of("reflectivelyAssignableToField($1S)", fieldName);
        return this;
    }

    ItemAssignableFactoryMethodBuilder unassignable() {
        assignableFromItemMethodCall = CodeBlock.of("unassignable()");
        return this;
    }

    ItemAssignableFactoryMethodBuilder assignableToField(String fieldName,
                                                         String getterName,
                                                         String setterName) {
        assignableFromItemMethodCall = CodeBlock.builder()
                                                .add("assignableToField($1S,", fieldName)
                                                .indent()
                                                .add("\n$1L,", methodRef(parentClassName, getterName))
                                                .add("\n$1L)", methodRef(parentClassName, setterName))
                                                .unindent()
                                                .build();
        return this;
    }

    ItemAssignableFactoryMethodBuilder assignableToCollection(String collectionFieldName,
                                                              String collectionGetterName,
                                                              String collectionSetterName,
                                                              ClassName collectionClassName) {
        assignableFromItemMethodCall = CodeBlock.builder()
                                                .add("assignableToCollection($1S,", collectionFieldName)
                                                .indent()
                                                .add("\n$1L,", methodRef(parentClassName, collectionGetterName))
                                                .add("\n$1L,", methodRef(parentClassName, collectionSetterName))
                                                .add("\n$1L)", format("%s::new", collectionClassName))
                                                .unindent()
                                                .build();
        return this;
    }

    private static String methodRef(ClassName className, String methodName) {
        if (isNull(methodName)) {
            return "null";
        }
        return format("%s::%s", className, methodName);
    }

    MethodSpec buildMethod() {
        return spec.addCode("return ")
                   .addStatement(CodeBlock.builder()
                                          .add(createItemCode).add("\n")
                                          .indent().indent()
                                          .add(".").add(assignableFromItemMethodCall)
                                          .unindent().unindent()
                                          .build())
                   .build();
    }

    MethodSpec buildMethodWithNestedFields() {

        spec.addAnnotation(SafeVarargs.class);
        spec.addParameter(ArrayTypeName.of(ParameterizedTypeName.get(ASSIGNABLE_CLASS_NAME,
                                                                     itemTypeName.box(),
                                                                     WildcardTypeName.subtypeOf(OBJECT))), "fields").varargs();
        return spec.addCode("return ")
                   .addStatement(CodeBlock.builder()
                                          .add(createItemCode).add("\n")
                                          .indent().indent()
                                          .add(".").add(assignableFromItemMethodCall)
                                          .add("\n")
                                          .add(".withFields(fields)")
                                          .unindent().unindent()
                                          .build())
                   .build();
    }
}
