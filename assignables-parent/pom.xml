<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>net.lepshi.commons</groupId>
    <artifactId>assignables-parent</artifactId>
    <version>0.0.5-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>${project.groupId} :: Assignables - Parent</name>
    <description>Parent POM for the Assignables project.</description>
    <url>https://gitlab.com/lepshi/commons-assignables</url>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>http://www.opensource.org/licenses/mit-license.php</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Juraj Oprsal</name>
            <email>juraj.oprsal@gmail.com</email>
            <url>https://gitlab.com/juraj.oprsal</url>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git@gitlab.com:lepshi/commons-assignables.git</connection>
        <developerConnection>scm:git:https://gitlab.com/lepshi/commons-assignables.git</developerConnection>
        <url>https://gitlab.com/lepshi/commons-assignables</url>
        <tag>HEAD</tag>
    </scm>

    <issueManagement>
        <url>https://gitlab.com/lepshi/commons-assignables/issues</url>
    </issueManagement>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <name>Sonatype OSSRH Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <name>Sonatype OSSRH Staging</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <mockito-core.version>2.28.2</mockito-core.version>
        <junit.version>5.3.1</junit.version>
        <guava.version>28.0-jre</guava.version>
        <hamcrest.version>1.3</hamcrest.version>
        <lombok.version>1.18.4</lombok.version>
        <apache-commons.version>3.9</apache-commons.version>
        <javapoet.version>1.11.1</javapoet.version>

        <surefire.plugin.version>2.22.2</surefire.plugin.version>
        <failsafe.plugin.version>2.22.2</failsafe.plugin.version>
        <compiler.plugin.version>3.5.1</compiler.plugin.version>
        <auto-service.plugin.version>1.0-rc6</auto-service.plugin.version>
        <source.plugin.version>3.2.0</source.plugin.version>
        <javadoc.plugin.version>3.1.1</javadoc.plugin.version>
        <gpg.plugin.version>1.6</gpg.plugin.version>
        <nexus-staging.plugin.version>1.6.8</nexus-staging.plugin.version>
        <scm.plugin.version>1.11.2</scm.plugin.version>

        <maven.deploy.skip>true</maven.deploy.skip>
        <!--        <gpg.passphrase>the_pass_phrase</gpg.passphrase>-->
    </properties>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${surefire.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>${failsafe.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${compiler.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${source.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${javadoc.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>${gpg.plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.sonatype.plugins</groupId>
                    <artifactId>nexus-staging-maven-plugin</artifactId>
                    <version>${nexus-staging.plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-scm-plugin</artifactId>
                    <version>${scm.plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-source-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <configuration>
                            <quiet>true</quiet>
                        </configuration>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                            <autoDropAfterRelease>false</autoDropAfterRelease>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <!-- For some strange reason this profile won't work when placed in settings.xml -->
            <id>intellij-javadoc-fix</id>
            <build>
                <pluginManagement>
                    <plugins>
                        <plugin>
                            <artifactId>maven-javadoc-plugin</artifactId>
                            <configuration>
                                <javadocExecutable>/wrk/java/jdk1.8.0_51/bin/javadoc</javadocExecutable>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
    </profiles>


    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>net.lepshi.commons</groupId>
                <artifactId>assignables-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.lepshi.commons</groupId>
                <artifactId>assignables-annotations</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.lepshi.commons</groupId>
                <artifactId>assignables-acceptance-tests</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito-core.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${apache-commons.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <dependency>
                <groupId>com.squareup</groupId>
                <artifactId>javapoet</artifactId>
                <version>${javapoet.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service</artifactId>
                <version>${auto-service.plugin.version}</version>
            </dependency>

            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-engine</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-library</artifactId>
                <version>${hamcrest.version}</version>
            </dependency>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

</project>