package net.lepshi.commons.assignables;

public class ItemAssignmentException extends RuntimeException {

    public ItemAssignmentException(String message) {
        super(message);
    }

    public ItemAssignmentException(String message, Throwable cause) {
        super(message, cause);
    }
}
