package net.lepshi.commons.assignables.utils;

import static java.lang.String.format;

public class Preconditions {

    public static <T> T checkNotNull(T obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        return obj;
    }

    public static <T> T checkNotNull(T obj, String msgTemplate, Object... args) {
        if (obj == null) {
            throw new NullPointerException(format(msgTemplate, args));
        }
        return obj;
    }

    public static void checkState(boolean isOk, String msgTemplate, Object... args) {
        if (!isOk) {
            throw new IllegalStateException(format(msgTemplate, args));
        }
    }

}
