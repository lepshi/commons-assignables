package net.lepshi.commons.assignables;

import java.util.Collection;
import java.util.function.Supplier;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static net.lepshi.commons.assignables.utils.Preconditions.checkNotNull;
import static org.mockito.Mockito.when;

class CollectionItemAssigner<I, C extends Collection<I>, P> extends ItemAssigner<P, I> {

    private final AssignableFactory.GetterMethod<P, C> collectionGetter;
    private final AssignableFactory.SetterMethod<P, C> collectionSetter;
    private final Supplier<C>                          collectionFactory;

    CollectionItemAssigner(I item,
                           String parentCollectionField,
                           AssignableFactory.GetterMethod<P, C> collectionGetter,
                           AssignableFactory.SetterMethod<P, C> collectionSetter,
                           Supplier<C> collectionFactory) {
        super(item, parentCollectionField);
        this.collectionFactory = checkNotNull(collectionFactory);
        this.collectionGetter = checkNotNull(collectionGetter);
        this.collectionSetter = collectionSetter;
    }

    @Override
    protected void assignToRealParent(P parent) {
        C items = collectionGetter.getFrom(parent);
        if (isNull(items)) {
            checkNotNull(collectionSetter,
                         "No setter method available for field %s#%s",
                         parent.getClass().getName(),
                         parentFieldName);
            items = collectionFactory.get();
            collectionSetter.setOn(parent, items);
        }
        items.add(item);
    }

    @Override
    protected void assignToMockParent(P parentMock) {
        final C oldItems = collectionGetter.getFrom(parentMock);
        final C newItems = collectionFactory.get();
        if (nonNull(oldItems)) {
            newItems.addAll(oldItems);
        }
        newItems.add(item);
        when(collectionGetter.getFrom(parentMock)).thenReturn(newItems);
    }
}
