package net.lepshi.commons.assignables;

import java.lang.reflect.Method;

import static net.lepshi.commons.assignables.utils.Preconditions.checkNotNull;
import static org.apache.commons.lang3.reflect.MethodUtils.getMatchingMethod;


class ReflectionsHelper {

    static Method objGetter(Class<?> objType, String field) {
        final String getterName = "get" + capitalFirstLetter(field);
        final Method getter = getMatchingMethod(objType,
                                                getterName);
        return checkNotNull(getter,
                            "Getter method '%s' not found on type: %s",
                            getterName,
                            objType);
    }

    static Method objSetter(Class<?> objType, String field, Class<?> fieldType) {
        final String setterName = "set" + capitalFirstLetter(field);
        final Method setter = getMatchingMethod(objType,
                                                setterName,
                                                fieldType);
        return checkNotNull(setter,
                            "Setter method '%s(%s)' not found on type: %s",
                            setterName,
                            fieldType.getName(),
                            objType.getName());
    }

    static String capitalFirstLetter(String text) {
        final String firstLetter = text.substring(0, 1);
        final String rest = text.substring(1);
        return firstLetter.toUpperCase() + rest;
    }
}
