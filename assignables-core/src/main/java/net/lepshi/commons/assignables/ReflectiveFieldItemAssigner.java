package net.lepshi.commons.assignables;

import org.mockito.internal.invocation.DefaultInvocationFactory;
import org.mockito.internal.stubbing.InvocationContainerImpl;
import org.mockito.invocation.Invocation;
import org.mockito.invocation.InvocationFactory;
import org.mockito.invocation.MockHandler;
import org.mockito.mock.MockCreationSettings;

import java.lang.reflect.Method;

import static org.mockito.internal.util.MockUtil.getMockHandler;
import static org.mockito.internal.util.MockUtil.getMockSettings;

class ReflectiveFieldItemAssigner<I, P> extends ItemAssigner<P, I> {

    ReflectiveFieldItemAssigner(I item, String parentFieldName) {
        super(item, parentFieldName);
    }

    @Override
    protected void assignToMockParent(Object parentMock) {
        final MockCreationSettings mockSettings = getMockSettings(parentMock);
        final Class actualParentType = mockSettings.getTypeToMock();
        final Method parentGetMethod = ReflectionsHelper.objGetter(actualParentType, parentFieldName);

        final MockHandler<Object> parentMockHandler = getMockHandler(parentMock);

        final Invocation getItemInvocation = new DefaultInvocationFactory().createInvocation(parentMock,
                                                                                             mockSettings,
                                                                                             parentGetMethod,
                                                                                             (InvocationFactory.RealMethodBehavior) () -> item /*not really used*/);

        try {
            parentMockHandler.handle(getItemInvocation);
        } catch (Throwable t) {
            throw new ItemAssignmentException("Mock invocation failed", t);
        }

        final InvocationContainerImpl invocationContainer = (InvocationContainerImpl) parentMockHandler.getInvocationContainer();
        invocationContainer.addAnswer(invocation -> item, false, null);
    }

    @Override
    protected void assignToRealParent(Object parent) throws Exception {
        ReflectionsHelper.objSetter(parent.getClass(), parentFieldName, item.getClass())
                         .invoke(parent, item);
    }
}
