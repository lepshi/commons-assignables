package net.lepshi.commons.assignables;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

import static java.lang.Thread.currentThread;


public class AssignableFactory {

    private AssignableFactory() {
        // not instantiable
    }

    public static <I> ItemAssignableFactory<I> item(I value) {
        return new ItemAssignableFactory<>(value);
    }

    public static <K, V> MapEntryAssignableFactory<K, V> mapEntry(K key, V value) {
        return new MapEntryAssignableFactory<>(key, value);
    }


    public static class ItemAssignableFactory<I> {
        private final I value;

        ItemAssignableFactory(I value) {
            this.value = value;
        }

        public <P> Assignable<P, I> assignableToField(String fieldName,
                                                      GetterMethod<P, I> getter,
                                                      SetterMethod<P, I> setter) {
            return new Assignable<>(new FieldItemAssigner<>(value,
                                                            fieldName,
                                                            getter,
                                                            setter));
        }

        public <P, C extends Collection<I>> Assignable<P, I> assignableToCollection(String collectionFieldName,
                                                                                    GetterMethod<P, C> collectionGetter,
                                                                                    SetterMethod<P, C> collectionSetter,
                                                                                    Supplier<C> collectionFactory) {
            return new Assignable<>(new CollectionItemAssigner<>(value,
                                                                 collectionFieldName,
                                                                 collectionGetter,
                                                                 collectionSetter,
                                                                 collectionFactory));
        }

        public Assignable<NoParent, I> unassignable() {
            return new Assignable<>(new NoParentAssigner<>(value));
        }

        public <P> Assignable<P, I> reflectivelyAssignableToField(String fieldName) {
            return new Assignable<>(new ReflectiveFieldItemAssigner<>(value,
                                                                      fieldName));
        }

        public <P> Assignable<P, I> reflectivelyAssignableToCollection(String collectionFieldName,
                                                                       Supplier<Collection<I>> collectionFactory) {
            return new Assignable<>(new ReflectiveCollectionItemAssigner<>(value,
                                                                           collectionFieldName,
                                                                           collectionFactory));
        }

        public <P> Assignable<P, I> autoAssignableToField() {
            return reflectivelyAssignableToField(getCallerMethodName());
        }

        private static String getCallerMethodName() {
            final StackTraceElement[] stackTrace = currentThread().getStackTrace();
            return stackTrace[3].getMethodName();
        }
    }

    public static class MapEntryAssignableFactory<K, V> {
        private final K key;
        private final V value;

        MapEntryAssignableFactory(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public <P, M extends Map<K, V>> Assignable<P, Map.Entry<K, V>> assignableToMap(String mapFieldName,
                                                                                       GetterMethod<P, M> mapGetter,
                                                                                       SetterMethod<P, M> mapSetter,
                                                                                       Supplier<M> mapFactory) {
            return new Assignable<>(new MapItemAssigner<>(Pair.of(key, value),
                                                          mapFieldName,
                                                          mapGetter,
                                                          mapSetter,
                                                          mapFactory));
        }

        //TODO
//        public <P, M extends Map<K, V>> Assignable<P, V> to(String mapFieldName,
//                                                                          GetterMethod<P, M> mapGetter,
//                                                                          SetterMethod<P, M> mapSetter,
//                                                                          Supplier<M> mapFactory) {
//            MapItemAssigner<P, K, V, M> mapItemAssigner = new MapItemAssigner<>(Pair.of(key, value),
//                                                                                    mapFieldName,
//                                                                                    mapGetter,
//                                                                                    mapSetter,
//                                                                                    mapFactory);
//            Assignable<P, Object> assignable = new Assignable<>(mapItemAssigner);
//            return assignable;
//        }
    }

    @FunctionalInterface
    public interface GetterMethod<T, V> {
        V getFrom(T instance);
    }

    @FunctionalInterface
    public interface SetterMethod<T, V> {
        void setOn(T instance, V value);
    }
}
