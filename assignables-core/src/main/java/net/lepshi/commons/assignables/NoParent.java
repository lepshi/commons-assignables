package net.lepshi.commons.assignables;

/**
 * Can be used as parent type of an {@link Assignable} to signify that it cannot be assigned to any parent.
 */
public final class NoParent {
    private NoParent() {
        //not instantiable
    }
}
