package net.lepshi.commons.assignables;

import java.util.Map;
import java.util.function.Supplier;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static net.lepshi.commons.assignables.utils.Preconditions.checkNotNull;
import static org.mockito.Mockito.when;

class MapItemAssigner<P, K, V, M extends Map<K, V>> extends ItemAssigner<P, Map.Entry<K, V>> {

    private final AssignableFactory.GetterMethod<P, M> mapGetter;
    private final AssignableFactory.SetterMethod<P, M> mapSetter;
    private final Supplier<M>                          mapFactory;

    MapItemAssigner(Map.Entry<K, V> item,
                    String parentMapField,
                    AssignableFactory.GetterMethod<P, M> mapGetter,
                    AssignableFactory.SetterMethod<P, M> mapSetter,
                    Supplier<M> mapFactory) {
        super(item, parentMapField);
        this.mapFactory = checkNotNull(mapFactory);
        this.mapGetter = checkNotNull(mapGetter);
        this.mapSetter = mapSetter;
    }

    @Override
    protected void assignToRealParent(P parent) {
        M items = mapGetter.getFrom(parent);
        if (isNull(items)) {
            checkNotNull(mapSetter,
                         "No setter method available for field %s#%s",
                         parent.getClass().getName(),
                         parentFieldName);
            items = mapFactory.get();
            mapSetter.setOn(parent, items);
        }
        items.put(item.getKey(),
                  item.getValue());
    }

    @Override
    protected void assignToMockParent(P parentMock) {
        final M oldItems = mapGetter.getFrom(parentMock);
        final M newItems = mapFactory.get();
        if (nonNull(oldItems)) {
            newItems.putAll(oldItems);
        }
        newItems.put(item.getKey(),
                     item.getValue());
        when(mapGetter.getFrom(parentMock)).thenReturn(newItems);
    }
}
