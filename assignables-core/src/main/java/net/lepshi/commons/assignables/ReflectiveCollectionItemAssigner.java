package net.lepshi.commons.assignables;

import org.mockito.internal.invocation.DefaultInvocationFactory;
import org.mockito.internal.stubbing.InvocationContainerImpl;
import org.mockito.invocation.Invocation;
import org.mockito.invocation.InvocationFactory;
import org.mockito.invocation.MockHandler;
import org.mockito.mock.MockCreationSettings;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.function.Supplier;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.reflect.MethodUtils.getMatchingMethod;
import static org.mockito.internal.util.MockUtil.getMockHandler;
import static org.mockito.internal.util.MockUtil.getMockSettings;

class ReflectiveCollectionItemAssigner<I, C extends Collection<I>, P> extends ItemAssigner<P, I> {

    private final Supplier<C> collectionCreator;

    ReflectiveCollectionItemAssigner(I item, String parentCollectionFieldName, Supplier<C> collectionCreator) {
        super(item, parentCollectionFieldName);
        this.collectionCreator = collectionCreator;
    }

    @Override
    protected void assignToMockParent(Object parentMock) throws Exception {
        final MockCreationSettings mockSettings = getMockSettings(parentMock);
        final Class actualParentType = mockSettings.getTypeToMock();
        final Method parentFieldGetter = getMatchingMethod(actualParentType,
                                                           "get" + ReflectionsHelper.capitalFirstLetter(parentFieldName));

        final MockHandler<Object> parentMockHandler = getMockHandler(parentMock);

        final Invocation getItemInvocation = new DefaultInvocationFactory().createInvocation(parentMock,
                                                                                             mockSettings,
                                                                                             parentFieldGetter,
                                                                                             (InvocationFactory.RealMethodBehavior) () -> item /*not really used*/);


        final C oldCollection = cast(parentFieldGetter.invoke(parentMock));
        final C newCollection = collectionCreator.get();
        newCollection.addAll(oldCollection);
        newCollection.add(item);

        final InvocationContainerImpl invocationContainer = (InvocationContainerImpl) parentMockHandler.getInvocationContainer();
        invocationContainer.addAnswer(invocation -> newCollection, false, null);
    }

    @Override
    protected void assignToRealParent(Object parent) throws Exception {
        final Method parentFieldGetter = ReflectionsHelper.objGetter(parent.getClass(), parentFieldName);
        C itemCollection = cast(parentFieldGetter.invoke(parent));
        if (isNull(itemCollection)) {
            itemCollection = collectionCreator.get();
            ReflectionsHelper.objSetter(parent.getClass(),
                                        parentFieldName,
                                        itemCollection.getClass())
                             .invoke(parent, itemCollection);
        }
        itemCollection.add(item);
    }

    @SuppressWarnings("unchecked")
    private static <T> T cast(Object obj) {
        return (T) obj;
    }
}
