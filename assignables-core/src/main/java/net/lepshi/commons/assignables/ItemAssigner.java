package net.lepshi.commons.assignables;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static net.lepshi.commons.assignables.utils.Preconditions.checkState;
import static org.mockito.internal.util.MockUtil.isMock;

abstract class ItemAssigner<P, I> {

    final I      item;
    final String parentFieldName;

    ItemAssigner(I item, String parentFieldName) {
        this.item = item;
        this.parentFieldName = parentFieldName;
    }

    I getItem() {
        return item;
    }

    final void assignToParent(P parent) {
        checkState(nonNull(parentFieldName),
                   "No parent field name specified for Assignable<%s, %s>",
                   item.getClass().getName(),
                   parent.getClass().getName());

        try {
            if (isMock(parent)) {
                assignToMockParent(parent);
            } else {
                assignToRealParent(parent);
            }
        } catch (Exception e) {
            throw new ItemAssignmentException(format("Error assigning item to its parent %s#%s (%s)",
                                                     parent.getClass().getName(),
                                                     parentFieldName,
                                                     item.getClass().getName()),
                                              e);
        }
    }

    protected abstract void assignToRealParent(P parent) throws Exception;

    protected abstract void assignToMockParent(P parentMock) throws Exception;
}
