package net.lepshi.commons.assignables;

import static java.lang.String.format;

class NoParentAssigner<I, P> extends ItemAssigner<P, I> {

    NoParentAssigner(I item) {
        super(item, "__no_parent_allowed__");
    }

    @Override
    protected void assignToRealParent(P parent) {
        throw unassignableError();
    }

    @Override
    protected void assignToMockParent(P parentMock) {
        throw unassignableError();
    }


    private ItemAssignmentException unassignableError() {
        return new ItemAssignmentException(format("This item cannot be assigned to any parent (%s)",
                                                  item.getClass().getName()));
    }
}
