package net.lepshi.commons.assignables;

import static java.util.Arrays.stream;


/**
 * {@link Assignable} is an item wrapper able to bind the item to a parent object's field.
 * <p>
 * Since the underlying binding mechanism uses Java reflection, the item can be bound to
 * any object with a matching field (requires casting to appropriate generic type).
 * <p>
 * Binding to Mockito mocks is also supported.
 *
 * @param <P> Type of parent to which the item can be assigned
 * @param <I> Type of the wrapped item
 */
public class Assignable<P, I> {

    private final ItemAssigner<P, I> itemAssigner;

    Assignable(ItemAssigner<P, I> itemAssigner) {
        this.itemAssigner = itemAssigner;
    }


    public I getItem() {
        return itemAssigner.getItem();
    }

    public <G> Assignable<G, I> as(Assignable<G, I> other) {
        return AssignableFactory.item(itemAssigner.getItem()).reflectivelyAssignableToField(other.itemAssigner.parentFieldName);
    }

    @SafeVarargs
    public final Assignable<P, I> withFields(Assignable<I, ?>... fields) {
        stream(fields).forEach(field -> field.assignToParent(getItem()));
        return this;
    }

    public void assignToParent(Assignable<?, P> parentAssignable) {
        assignToParent(parentAssignable.getItem());
    }

    public void assignToParent(P parent) {
        itemAssigner.assignToParent(parent);
    }

}
