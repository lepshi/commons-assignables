package net.lepshi.commons.assignables;

import static net.lepshi.commons.assignables.utils.Preconditions.checkNotNull;
import static org.mockito.Mockito.when;

class FieldItemAssigner<I, P> extends ItemAssigner<P, I> {

    private final AssignableFactory.GetterMethod<P, I> fieldGetter;
    private final AssignableFactory.SetterMethod<P, I> fieldSetter;

    FieldItemAssigner(I item,
                      String parentFieldName,
                      AssignableFactory.GetterMethod<P, I> fieldGetter,
                      AssignableFactory.SetterMethod<P, I> fieldSetter) {
        super(item, parentFieldName);
        this.fieldGetter = checkNotNull(fieldGetter);
        this.fieldSetter = fieldSetter;
    }

    @Override
    protected void assignToRealParent(P parent) {
        fieldSetter.setOn(parent, item);
    }

    @Override
    protected void assignToMockParent(P parentMock) {
        when(fieldGetter.getFrom(parentMock)).thenReturn(item);
    }
}
