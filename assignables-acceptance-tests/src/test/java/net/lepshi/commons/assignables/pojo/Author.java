package net.lepshi.commons.assignables.pojo;

import net.lepshi.commons.assignables.annotations.Assignables;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Assignables
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    private int               id;
    private String            name;
    private Map<String, Book> booksByName;
}
