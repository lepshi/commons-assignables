package net.lepshi.commons.assignables.pojo;

import net.lepshi.commons.assignables.annotations.Assignables;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Assignables
public class Book {
    private int          id;
    private String       title;
    private Set<Author>  authors;
    private List<Review> reviews;
}
