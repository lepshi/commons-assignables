package net.lepshi.commons.assignables;

import net.lepshi.commons.assignables.pojo.AssignableAuthor;
import net.lepshi.commons.assignables.pojo.AssignableBook;
import net.lepshi.commons.assignables.pojo.AssignableReview;
import net.lepshi.commons.assignables.pojo.Book;
import net.lepshi.commons.assignables.pojo.Review;
import org.junit.jupiter.api.Test;

import static net.lepshi.commons.assignables.pojo.AssignableAuthor.name;
import static net.lepshi.commons.assignables.pojo.AssignableBook._book;
import static net.lepshi.commons.assignables.pojo.AssignableBook.oneOfAuthors;
import static net.lepshi.commons.assignables.pojo.AssignableBook.oneOfReviews;
import static net.lepshi.commons.assignables.pojo.AssignableBook.title;
import static net.lepshi.commons.assignables.pojo.AssignableReview.text;

class GeneratedAssignablesTest {

    @Test
    void should_build_book_struct_from_generated_assignables() {

        Assignable<Book, Review> oneReview = oneOfReviews();

        Assignable<NoParent, Book> book = _book(AssignableBook.id(1),
                                                title("Bible"),
                                                oneOfAuthors(AssignableAuthor.id(2),
                                                             name("John")),
                                                oneOfAuthors(AssignableAuthor.id(3),
                                                             name("Paul")),
//                                                             booksByName().key("Bible")
//                                                                          .value(AssignableBook.id(1),
//                                                                                 title("abc"))),
                                                oneReview.withFields(AssignableReview.id(4),
                                                                     text("a hell good read")),
                                                oneOfReviews(AssignableReview.id(5),
                                                             text("opinions too conservative")));

//        book.as(AssignableReview.book()).assignToParent(oneReview);
        oneReview.withFields(book.as(AssignableReview.book()));
    }
}
