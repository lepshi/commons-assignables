package net.lepshi.commons.assignables.pojo;

import net.lepshi.commons.assignables.annotations.Assignables;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Assignables
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Review {
    private int    id;
    private String text;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Book   book;

    public String getReadonlyText() {
        return "";
    }
}
