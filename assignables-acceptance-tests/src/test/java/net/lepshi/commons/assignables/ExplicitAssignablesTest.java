package net.lepshi.commons.assignables;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import net.lepshi.commons.assignables.pojo.Author;
import net.lepshi.commons.assignables.pojo.Book;
import net.lepshi.commons.assignables.pojo.Review;
import lombok.RequiredArgsConstructor;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Iterables.get;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableAuthor.name;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.author;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.emptyAuthors;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.emptyReviews;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.mockAuthor;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.mockReview;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.review;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableBook.title;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.AssignableReview.text;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.BookMatcher.EXPECTED_OWNER_BOOK_STUB;
import static net.lepshi.commons.assignables.ExplicitAssignablesTest.BookMatcher.bookWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.util.MockUtil.isMock;


class ExplicitAssignablesTest {

    @Test
    void should_bind_simple_item_to_real_parent() {
        final Book book = new Book();
        final Author author = new Author();

        AssignableBook.id(123).assignToParent(book);
        AssignableAuthor.id(456).assignToParent(author);

        assertThat(book.getId(), is(123));
        assertThat(author.getId(), is(456));
    }

    @Test
    void should_bind_simple_item_to_mock_parent() {
        final Book book = mock(Book.class);
        final Author author = mock(Author.class);

        AssignableBook.id(123).assignToParent(book);
        AssignableAuthor.id(456).assignToParent(author);

        assertThat(book.getId(), is(123));
        assertThat(author.getId(), is(456));
    }

    @Test
    void should_compose_parent_from_field_items() {
        final Assignable<NoParent, Book> book = AssignableBook._root(AssignableBook.id(10),
                                                                     title("Bible"),
                                                                     author(idByReflection(20), name("John")),
                                                                     author(AssignableAuthor.id(21), name("Luke")),
                                                                     review(idByReflection(30), text("God is great")),
                                                                     review(AssignableReview.id(31), text("too conservative")));

        MatcherAssert.assertThat(book.getItem(), Matchers.is(bookWith(10, "Bible",
                                                                      ImmutableSet.of(new Author(20, "John", null),
                                                                        new Author(21, "Luke", null)),
                                                                      ImmutableList.of(new Review(30, "God is great", null),
                                                                         new Review(31, "too conservative", null)))));
    }

    @Test
    void should_allow_building_cyclic_structures() {
        final Assignable<Review, Book> book = AssignableReview.book(AssignableBook.id(0),
                                                                    title("Book with reviews"));
        final Assignable<Book, Review> review1 = review(AssignableReview.id(1),
                                                        text("1st review"),
                                                        book);
        final Assignable<Book, Review> review2 = review(AssignableReview.id(2),
                                                        text("2nd review"),
                                                        book);
        final Assignable<Book, Review> review3 = review(AssignableReview.id(3),
                                                        text("3rd review"),
                                                        book);

        book.withFields(review1,
                        review2);
        review3.assignToParent(book);

        MatcherAssert.assertThat(book.getItem(), Matchers.is(bookWith(0, "Book with reviews",
                                                                      null,
                                                                      asList(new Review(1, "1st review", EXPECTED_OWNER_BOOK_STUB),
                                                               new Review(2, "2nd review", EXPECTED_OWNER_BOOK_STUB),
                                                               new Review(3, "3rd review", EXPECTED_OWNER_BOOK_STUB)))));
    }

    @Test
    void should_allow_setting_empty_collections() {
        final Assignable<?, Book> book = AssignableBook._root(AssignableBook.id(0),
                                                              title("Book with reviews"),
                                                              emptyAuthors(),
                                                              emptyReviews());

        MatcherAssert.assertThat(book.getItem(), Matchers.is(bookWith(0, "Book with reviews",
                                                                      emptySet(),
                                                                      emptyList())));
    }

    @Test
    void should_compose_mock_parent_from_field_items() {
        final Assignable<NoParent, Book> book = AssignableBook._mockRoot(AssignableBook.id(10),
                                                                         title("Bible"),
                                                                         mockAuthor(AssignableAuthor.id(20), name("John")),
                                                                         author(AssignableAuthor.id(21), name("Luke")),
                                                                         mockReview(AssignableReview.id(30), text("God is great")),
                                                                         mockReview(AssignableReview.id(31), text("too conservative")));

        assertThat(isMock(book.getItem()), is(true));
        MatcherAssert.assertThat(book.getItem().getId(), is(10));
        MatcherAssert.assertThat(book.getItem().getTitle(), is("Bible"));

        final Set<Author> authors = book.getItem().getAuthors();
        assertThat(authors, hasSize(2));
        assertThat(get(authors, 0).getId(), is(20));
        assertThat(get(authors, 0).getName(), is("John"));
        assertThat(get(authors, 1).getId(), is(21));
        assertThat(get(authors, 1).getName(), is("Luke"));

        final List<Review> reviews = book.getItem().getReviews();
        assertThat(reviews, hasSize(2));
        assertThat(get(reviews, 0).getId(), is(30));
        assertThat(get(reviews, 0).getText(), is("God is great"));
        assertThat(get(reviews, 1).getId(), is(31));
        assertThat(get(reviews, 1).getText(), is("too conservative"));
    }


    static <P> Assignable<P, Integer> idByReflection(int id) {
        return AssignableFactory.item(id).reflectivelyAssignableToField("id");
    }

    static class AssignableBook {

        static Assignable<NoParent, Book> _root(Book book) {
            return AssignableFactory.item(book).reflectivelyAssignableToField("_no_parent_");
        }

        @SafeVarargs
        static Assignable<NoParent, Book> _root(Assignable<Book, ?>... fieldItems) {
            return _root(new Book()).withFields(fieldItems);
        }

        @SafeVarargs
        static Assignable<NoParent, Book> _mockRoot(Assignable<Book, ?>... fieldItems) {
            return _root(mock(Book.class)).withFields(fieldItems);
        }

        static Assignable<Book, Integer> id(int id) {
            return AssignableFactory.item(id).autoAssignableToField();
        }

        static Assignable<Book, String> title(String title) {
            return AssignableFactory.item(title).autoAssignableToField();
        }


        static Assignable<Book, Author> author(Author author) {
            return AssignableFactory.item(author).reflectivelyAssignableToCollection("authors", LinkedHashSet::new);
        }

        @SafeVarargs
        static Assignable<Book, Author> author(Assignable<Author, ?>... fieldItems) {
            return author(new Author()).withFields(fieldItems);
        }

        @SafeVarargs
        static Assignable<Book, Author> mockAuthor(Assignable<Author, ?>... fieldItems) {
            return AssignableFactory.item(mock(Author.class)).assignableToCollection("authors",
                                                                                     Book::getAuthors,
                                                                                     Book::setAuthors,
                                                                                     LinkedHashSet::new)
                                    .withFields(fieldItems);
        }

        static Assignable<Book, Set<Author>> emptyAuthors() {
            return AssignableFactory.item((Set<Author>) new LinkedHashSet<Author>()).assignableToField("authors",
                                                                                                       Book::getAuthors,
                                                                                                       Book::setAuthors);
        }


        @SafeVarargs
        static Assignable<Book, Review> review(Assignable<Review, ?>... fieldItems) {
            return AssignableFactory.item(new Review()).<Book>reflectivelyAssignableToCollection("reviews", ArrayList::new)
                    .withFields(fieldItems);
        }

        @SafeVarargs
        static Assignable<Book, Review> mockReview(Assignable<Review, ?>... fieldItems) {
            return AssignableFactory.item(mock(Review.class)).<Book>reflectivelyAssignableToCollection("reviews",
                                                                                                       ArrayList::new)
                    .withFields(fieldItems);
        }

        static Assignable<Book, List<Review>> emptyReviews() {
            return AssignableFactory.item((List<Review>) new ArrayList<Review>()).reflectivelyAssignableToField("reviews");
        }
    }

    static class AssignableAuthor {

        static Assignable<Author, Integer> id(int id) {
            return AssignableFactory.item(id).autoAssignableToField();
        }

        static Assignable<Author, String> name(String name) {
            return AssignableFactory.item(name).autoAssignableToField();
        }
    }

    static class AssignableReview {

        static Assignable<Review, Integer> id(int id) {
            return AssignableFactory.item(id).autoAssignableToField();
        }

        static Assignable<Review, String> text(String text) {
            return AssignableFactory.item(text).autoAssignableToField();
        }

        @SafeVarargs
        static Assignable<Review, Book> book(Assignable<Book, ?>... fieldItems) {
            return AssignableFactory.item(new Book()).<Review>autoAssignableToField()
                    .withFields(fieldItems);
        }
    }


    @RequiredArgsConstructor
    static class BookMatcher extends BaseMatcher<Book> {

        static final Book EXPECTED_OWNER_BOOK_STUB = new Book();

        private final Book expectedBook;

        static Matcher<Book> bookWith(int id, String title, Set<Author> authors, List<Review> reviews) {
            final Book expectedBook = new Book(id, title, authors, reviews);
            reviews.forEach(review -> {
                if (review.getBook() == EXPECTED_OWNER_BOOK_STUB) {
                    review.setBook(expectedBook);
                }
            });
            return new BookMatcher(expectedBook);
        }

        @Override
        public boolean matches(Object item) {
            return expectedBook.equals(item);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(expectedBook.toString());
        }
    }

}